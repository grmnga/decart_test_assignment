class CurrenciesController < ApplicationController
  before_action :authenticate

  def index
    @currencies = if params[:page]
                    per_page = params[:per]? params[:per] : 10
                    Currency.page(params[:page]).per(per_page)
                  else
                    Currency.all
                  end
    render json: @currencies
  end

  def show
    @currency = Currency.find(params[:id])
    render json: @currency
  end

  private

    def authenticate
      authenticate_or_request_with_http_token do |token, options|
        ActiveSupport::SecurityUtils.secure_compare(token, ENV['SECRET_TOKEN'])
      end
    end
end

