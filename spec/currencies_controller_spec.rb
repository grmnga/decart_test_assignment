require "rails_helper"

RSpec.describe CurrenciesController, :type => :controller do
  before do
    request.headers["Authorization"] = authenticate_with_token(ENV['SECRET_TOKEN'])
  end
  let!(:currencies) { FactoryBot.create_list(:currency, 30) }

  describe "GET index" do
    it "response has all currencies if there is no 'page' param" do
      get :index
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq(currencies.size)
    end

    it "response has 10 items per page by default if there is the 'page' param" do
      get :index, params: { page: 1 }
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq(10)
    end

    it "response has right number of items per page if there is the 'per' param" do
      per = rand(1..30)
      get :index, params: { page: 1, per: per }
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq(per)
    end
  end

  describe "GET show" do
    let(:currency) { currencies.sample }

    before do
      request.headers["Authorization"] = authenticate_with_token(ENV['SECRET_TOKEN'])
      get :show, params: { id: currency.id }
    end

    it "response contains expected values" do
      json_response = JSON.parse(response.body)
      expect(json_response['id']).to eq(currency.id)
      expect(json_response['name']).to eq(currency.name)
      expect(json_response['rate']).to eq(currency.rate)
    end
  end

  def authenticate_with_token(token)
    ActionController::HttpAuthentication::Token.encode_credentials(token)
  end
end
