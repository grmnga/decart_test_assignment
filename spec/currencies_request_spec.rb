require "rails_helper"

RSpec.describe 'Currency API', :type => :request do
  let(:headers) { { "Authorization" => authenticate_with_token(ENV['SECRET_TOKEN']) } }
  let!(:currencies) { FactoryBot.create_list(:currency, 30) }

  describe "GET currencies_path" do
    context "when there is an invalid api token" do
      it "has 401 status code" do
        get currencies_path
        expect(response.status).to eq(401)
      end
    end

    context "when there is a valid api token" do
      before do
        get currencies_path, headers: headers
      end

      it "has 200 status code" do
        expect(response.status).to eq(200)
      end

      it "return JSON" do
        expect(response.content_type).to eq("application/json")
      end

      it "JSON body contains expected attributes" do
        json_response = JSON.parse(response.body)[0]
        expect(json_response.keys).to include('id', 'name', 'rate')
      end
    end
  end

  describe "GET /currencies/:id" do
    let(:currency) { currencies.sample }

    context "when there is an invalid api token" do
      it "has a 401 status code without authentication" do
        get currency_path(currency)
        expect(response.status).to eq(401)
      end
    end

    context "when there is a valid api token" do
      let(:headers) { { "Authorization" => authenticate_with_token(ENV['SECRET_TOKEN']) } }
      let!(:currencies) { FactoryBot.create_list(:currency, 30) }

      before do
        get currency_path(currency), headers: headers
      end

      it "has a 200 status code with authentication" do
        expect(response.status).to eq(200)
      end

      it "return JSON" do
        expect(response.content_type).to eq("application/json")
      end

      it "JSON body contains expected attributes" do
        json_response = JSON.parse(response.body)
        expect(json_response.keys).to include('id', 'name', 'rate')
      end
    end
  end

  def authenticate_with_token(token)
    ActionController::HttpAuthentication::Token.encode_credentials(token)
  end
end
