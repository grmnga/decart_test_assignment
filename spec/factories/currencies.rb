FactoryBot.define do
  factory :currency do
    name { ['Австралийский доллар',
            'Бразильский реал',
            'Сингапурский доллар',
            'Болгарский лев',
            'Румынский лей'].sample }
    rate { rand(1.0..100.0).round(2) }
  end
end