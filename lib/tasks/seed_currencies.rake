require 'open-uri'
require 'nokogiri'

namespace :setup do
  desc 'Seed or update rates'
  task :seed_currencies => :environment do
    url = "http://www.cbr.ru/scripts/XML_daily.asp"

    xml = Nokogiri::XML(open(url)).to_s
    valutes = Nokogiri::Slop(xml).ValCurs.Valute
    valutes.each do |valute|
      currency = Currency.find_or_create_by!(name: valute.Name.content)
      currency.update!(rate: valute.Value.content)
    end
  end
end
